clear, clc
format compact

% this file is used as an example for H-infinity controller design for a
% double harmonic oscillator

%% State-Space and Transfer Function

k1 = 5;
k2 = 0.01;
m1 = 0.5;
m2 = 0.5;
b1 = 0.5;
b2 = 1/2000;

A = [0 1 0 0;
     -(k1+k2)/m1 -(b1+b2)/m1 k2/m1 b2/m1;
     0 0 0 1;
     k2/m2 b2/m2 -k2/m2 -b2/m2];
B = [0; 1/m1;0;0];

C = [1 0 -1 0];
D = 0;

s = tf('s');
sys = ss(A,B,C,D);
G = tf(sys);

%% Plot the Frequency Response of G

% options for the Bode plot : change units
opts = bodeoptions;
opts.FreqUnits = 'Hz';
opts.MagUnits = 'abs';
opts.MagScale = 'log';

% bode plot of G(s)
figure(1), clf(1)
bode(G, opts)
title('Bode Plot of the Plant G(s)')

%% Disturbance Filters

% Filter to shape white noise to the spectrum of the actual sensor noise

Vn = tf([9.94004345e-13, 6.88255650e-12, 8.63710417e-14, 2.71219362e-16],...
    [1.0000000e+00, 1.8849556e-04, 1.1843525e-08, 2.4805020e-13]);

% plot frequency response
figure(2),  clf(2), bode(Vn, opts),hold on

Vd = tf([0,5.92856898488174e-08,7.84160152403009e-07,5.20436977605335e-06,1.82013763744689e-05,1.23173886425623e-05,...
    8.60697342166270e-06,2.68791349323640e-06,4.13531449857269e-07,3.61512070156043e-08,1.41502574915719e-09],...
    [1,18.4222993206505,46.1901039028566,59.8099599879608,65.2891742545302,41.4735240522369,18.6409313957479,4.97390644970040,...
    0.844410490846429,0.0385417953604305,0.000210034380292112]);

bode(Vd, opts)
%% Using a weigting filter on the motion of the mass, disturbance inputs

s = tf('s');                                                                 % Laplace Variable
% Weight on the performance output
W1 = ((s+0.1)^3 / (s + 0.5)^3);
W1 = W1 * (1/dcgain(W1));

W2 = ((s+2)*(s+1e-8)^2 / (s+100)^3);
W2 = 1e-22 * W2 * (1/dcgain(W2));


bodemag(G*W1, W1*G*Vd, W2, opts);
lgnd = legend('u->G*W1', 'w_gnd->W1*G*Vd', 'u->W2');
lgnd.FontSize = 20;
t_ = title('Contributions to the H2 norm given unit input');
t_.FontSize = 25;

%%

% Generalized plant
P = [W1*G*Vd, 0, W1*G;
     0, 0, W2;
     G*Vd, Vn, G];

P = minreal(ss(P));

% Controller Synthesis

nu=1; ny=1;
[K, CL, gamma] = h2syn(P,ny,nu);
gamma = gamma
K = tf(K);

% Closed Loop Transfer Functions
S = feedback(1,G*K);
GS = G*S;
T = 1-S;

figure(2), clf(2), hold on
bodemag(GS)
bodemag(G*K*S)
legend('GS', 'GKS')

title('Magnitude Plot of GS')

% Plot the step response of GS
figure(3), clf(3), hold on
impulse(GS)
legend('GS')
title('Impulse Response of GS')

figure(4), clf(4)
impulse(K*S)
legend('KS')
title('Impulse Response of KS')

%% Plot the TF: PS*d + T*n
CL = tf(CL);

% make a vector of frequencies
f = logspace(-5, 2, 1000);
w = 2*pi*f;

ASD_d = squeeze(abs(freqresp(G*S*Vd, w)));                                  % ASD contribution of d
ASD_n = squeeze(abs(freqresp(T*Vn, w)));                                    % ASD contribution of n
ASD = sqrt(ASD_d.^2 + ASD_n.^2);                                            % total noise spectrum

d = squeeze(abs(freqresp(Vd, w)));                                          % frequency response of d spectrum
n = squeeze(abs(freqresp(Vn, w)));                                          % frequency response of n spectrum
Gd = squeeze(abs(freqresp(G*Vd, w)));                                       % open-loop disturbance response


% plot the spectra of disturbance, sensor noise and total noise
figure(5), clf(5)
loglog(f, ASD, 'LineWidth', 2)                                              % total noise
hold on;
loglog(f, ASD_d, '--')                                                      % disturbance spectrum
hold on;
loglog(f, ASD_n, '--')                                                      % sensor noise spectrum
hold on;
loglog(f, Gd)                                                               % open loop disturbance response, no controller
hold on;
loglog(f, n)                                                                % sensor noise spectrum
grid on;
lgnd = legend('Total Noise', 'Vd cl', 'Vn cl','Disturbance ol', 'sensing noise');
lgnd.FontSize = 20;
xlabel('Frequency [Hz]'), ylabel('ASD | unit/Hz^{1/2}')
title('Noise Contributions')

%%

figure(6), clf(6)
plot(f, ASD, 'LineWidth', 2)                                              % total noise
hold on;
plot(f, Gd)                                                               % open loop disturbance response, no controller
hold on;
lgnd = legend('Total Noise', 'Disturbance ol');
lgnd.FontSize = 20;

