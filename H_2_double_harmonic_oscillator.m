clear, clc
close all
format compact

% this file is used as an example for H-2 optimal controller design for a
% double harmonic oscillator
% Authors: Sander Sijtsma (s.k.sijtsma@student.utwente.nl), Pooya Saffarieh (p.saffarieh@nikhef.nl)

%% State-Space and Transfer Function

k1 = 5;                                                                     % spring constant of spring 1
k2 = 0.01;                                                                  % spring constant of spring 2
m1 = 0.5;                                                                   % mass of frame 1
m2 = 0.5;                                                                   % mass of frame 2
b1 = 0.5;                                                                   % damping coefficient of damper 1
b2 = 1/2000;                                                                % damping coefficient of damper 2

% state space matrices
A = [0 1 0 0;
     -(k1+k2)/m1 -(b1+b2)/m1 k2/m1 b2/m1;
     0 0 0 1;
     k2/m2 b2/m2 -k2/m2 -b2/m2];
B = [0; 1/m1;0;0];

C = [1 0 -1 0];
D = 0;

% build the system
s = tf('s');                                                                % Laplace variable
sys = ss(A,B,C,D);                                                          % state space model of the system
G = tf(sys);                                                                % transfer function of the system

%% Options for Bode Plot

% options for the Bode plot : change units
opts = bodeoptions;
opts.FreqUnits = 'Hz';
opts.MagUnits = 'abs';
opts.MagScale = 'log';

%% Disturbance Filters

% Filter to shape white noise to the spectrum of the actual sensor noise

Vn = tf([9.94004345e-13, 6.88255650e-12, 8.63710417e-14, 2.71219362e-16],...
    [1.0000000e+00, 1.8849556e-04, 1.1843525e-08, 2.4805020e-13]);

% Filter to shape white noise to the spectrum of seismic noise
Vd = tf([0,5.92856898488174e-08,7.84160152403009e-07,5.20436977605335e-06,1.82013763744689e-05,1.23173886425623e-05,...
    8.60697342166270e-06,2.68791349323640e-06,4.13531449857269e-07,3.61512070156043e-08,1.41502574915719e-09],...
    [1,18.4222993206505,46.1901039028566,59.8099599879608,65.2891742545302,41.4735240522369,18.6409313957479,4.97390644970040,...
    0.844410490846429,0.0385417953604305,0.000210034380292112]);

%% Using a weigting filter on the motion of the mass, disturbance inputs

s = tf('s');                                                                % Laplace Variable

% Weight on the performance output
%%%%%%%%%%%%%%%% FILL THIS IN %%%%%%%%%%%%%%%
W1 = [];                                                                    % Change This Weigting Filter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot the Weighting Filter

figure(1), clf(1), hold on
bodemag(W1, opts)
grid on
legend('W1(s)')

%%
% Generalized plant
%%%%%%%%%%%%%%%%%%%% FILL THIS IN %%%%%%%%%%%%%%%%%%%%%%
P = [                                                                       % z1 = P(1,:)*[w u]'
      ];                                                                    % v  = P(2,:)*[w u]'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

P = ss(minreal(P));                                                         % remove superfluous states, convert to state-space

% Controller Synthesis
nu=1; ny=1;                                                                 % number of I/O
[K, CL, gamma] = h2syn(P,ny,nu);                                            % compute H2-optimal controller for P(s)
P_2norm = gamma                                                             % 2-norm of closed loop: this must be minimized
K = tf(K);                                                                  % convert controller to a transfer function object

% Closed Loop Transfer Functions
S = feedback(1,G*K);                                                        % sensitivity S(s)
GS = G*S;                                                                   % input sensitivity: GS = G/(1+GK) and y = GS*d
T = 1-S;                                                                    % complementary sensitivity T(s) = GK/(1+GK)

% plot GS and T
figure(2), clf(2), hold on
bodemag(GS)                                                                 % Bode plot of GS                                                               
bodemag(S)                                                                  % Bode plot of S  
grid on
legend('GS', 'S')
title('Magnitude Plot of GS and S')

% Plot the step response of GS: says something about the disturbance
% rejection
figure(3), clf(3), hold on
impulse(GS)                                                                 % impulse response of GS = G/(1+GK)
legend('GS')
title('Impulse Response of GS')

%% Plot the TF: PS*d + T*n

% make a vector of frequencies
f = logspace(-5, 2, 1000);                                                  % logaritmic space of frequencies : [1e-5, - 1e2]
w = 2*pi*f;                                                                 % vector of angular frequencies

ASD_d = squeeze(abs(freqresp(G*S*Vd, w)));                                  % ASD contribution of d
ASD_n = squeeze(abs(freqresp(T*Vn, w)));                                    % ASD contribution of n
ASD = sqrt(ASD_d.^2 + ASD_n.^2);                                            % total noise spectrum

d = squeeze(abs(freqresp(Vd, w)));                                          % frequency response of d spectrum
n = squeeze(abs(freqresp(Vn, w)));                                          % frequency response of n spectrum
Gd = squeeze(abs(freqresp(G*Vd, w)));                                       % open-loop disturbance response

% plot the spectra of disturbance, sensor noise and total noise
figure(5), clf(5)
loglog(f, ASD, 'LineWidth', 2)                                              % total noise
hold on;
loglog(f, ASD_d)                                                            % disturbance spectrum
hold on;
loglog(f, ASD_n)                                                            % sensor noise spectrum
hold on;
loglog(f, Gd)                                                               % open loop disturbance response, no controller
hold on;
loglog(f, n)                                                                % sensor noise spectrum
grid on;
legend('Total Noise', 'Vd cl', 'Vn cl','Disturbance ol', 'sensing noise')
xlabel('Frequency [Hz]'), ylabel('ASD | unit/Hz^{1/2}')
title('Noise Contributions')

