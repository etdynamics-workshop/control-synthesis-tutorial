%% Hinf example code
% Example code to illustrate Hinf synthesis for a single degree of freedom
% with representative dynamics for a GW detector longitudinal loop.

% Written by Mathyn van Dael, Eindhoven University of Technology/Nikhef, 2024

%% Clear variables and set bodeoptions.

clear; close all; clc;

opt = bodeoptions; 
opt.FreqUnits = 'Hz'; opt.PhaseWrapping = 'on'; opt.Grid = 'on'; opt.XLim = ([1e-2 1e2]);
opt.XLabel.FontSize = 16; opt.YLabel.FontSize = 16; opt.Title.FontSize = 16; 
opt.TickLabel.FontSize = 12; opt.InputLabels.FontSize = 12; opt.OutputLabels.FontSize = 12;

%% Define plant
% Use opto-mechanical plant as example with single harmonic oscillator plus
% optical plant containing a cavity pole
s = tf('s');

% Mechanical transfer functions
dcGain = 2e-6;
wn = 2*pi*0.7;
beta = 0.1;
A = dcGain*(wn^2)/(s^2+2*beta*wn*s+wn^2); % Suspension model

cavPole = 2*pi*500;
G = db2mag(165)*cavPole/(s+cavPole); 

%% Scale plant
% Plant requires scaling to 1 to allow for correct weighting on closed-loop
% transfer functions

Gsc = G*db2mag(-165); % Scale magnitude of optical plant to 1
Asc = A/dcGain; % Scale magnitude of actuator to 1

Tu = db2mag(165)*dcGain; % Rescaling value

%% Create state-space matrices
% Synthesis requires state-space models for synthesis since it is solved
% using either Ricatti or LMI's
A_ss = ss(Asc);
G_ss = ss(Gsc);

%% Create weighting filters

% For shot noise assume flat spectrum limiting error signal.

% 1/f^2 as seismic model, but use LP filter instead to avoid integrators in weighting function (condition for Hinf/H2 synthesis)
wn_seis = 2*pi*0.001; 
H_seis = wn_seis^2/(s^2+2*0.7*wn_seis*s+wn_seis^2);

% Assume addition unmodelled suspension model as (damped) stack of harmonic oscillators
wn_susp = 2*pi.*[0.1 0.3 0.8];
damp_susp = [0.1 0.1 0.1]; 

H_susp = 1;

for iStage = 1:length(wn_susp)
    H_susp = H_susp*(wn_susp(iStage)^2/(s^2+2*damp_susp(iStage)*wn_susp(iStage)*s+wn_susp(iStage)^2));
end

H_seis_susp = H_seis*H_susp; % Total seismic model

%% Create weighting filters

w_bw = 2*pi*10; % Set target bandwidth
bw_weighting = 5; % Scaling factor to determine how control roll-off start w.r.t. target bw. Reduce weight to provide more stringent requirements on roll-off

slope = length(wn_susp)*2+2; % Slope of ground motion coupling to mirror (H_seis*H_susp) above 10 Hz
modMargin = db2mag(6); % Required modulus margin (peak of sensitivity function)

% Weighting filter on sensitivity (for low frequencies)
w_d = H_seis_susp*((s^2+2*0.6*w_bw/2.5*s+(w_bw/2.5)^2)/(w_bw/2.5)^2)^(slope/2);
w_d = 1/abs((freqresp(w_d, 200, 'Hz')))*w_d*1/modMargin;

% Weighting for control effort
w_n = 1/(w_bw*bw_weighting)^2*(s^2+2*0.5*w_bw*bw_weighting*s+(w_bw*bw_weighting)^2);
lpfreq = 2*pi*500;
w_n = w_n*lpfreq^2/(s^2+2*0.7*lpfreq*s+lpfreq^2); % Add LP filter to weighting filter to ensure properness of filter (condition for Hinf/H2 synthesis)

% Create output weighting filter
W = tf(eye(3));
W(1,1) = w_d; W(2,2) = 1e-3; % Only apply weighting to sensitivity function
% W(1,1) = w_d; W(2,2) = w_n; % Also apply weighting on roll-off


%% Create generalized plant

eta = icsignal(1);
d = icsignal(1);
x = icsignal(1);
u = icsignal(1);
e = icsignal(1);
P = iconnect;
P.Input = [eta; d; u];
P.Output = [e; u; e];
P.Equation{1} = equate(e,-eta-G_ss*x);
P.Equation{2} = equate(x,d+A_ss*u);
P_noWeight = minreal(P.System);

% Create weighted plant
P_weighted = minreal(W*P_noWeight); % Make sure to have minimal realization of P for synthesis

%% Synthesize controller

[K_syn_sc, CL_wFilt, gamma] = hinfsyn(P_weighted, 1, 1);

CL_wFilt.InputName{1} = 'Sensor noise eta'; CL_wFilt.InputName{2} = 'Ground motion d';
CL_wFilt.OutputName{1} = 'Error signal e'; CL_wFilt.OutputName{2} = 'Control effort u';

K_syn = K_syn_sc/Tu; % Scale back controller to correct units

L_syn = G*A*K_syn; % Define open-loop of system

CL_syn = lft(P_noWeight, K_syn_sc, 1, 1); % Define closed-loop transfer functions;
CL_syn.InputName{1} = 'Sensor noise eta'; CL_syn.InputName{2} = 'Ground motion d';
CL_syn.OutputName{1} = 'Error signal e'; CL_syn.OutputName{2} = 'Control effort u';

%% Plot transfer functions

figure;
opt.Title.String = 'Open-loop transfer function';
bode(L_syn, opt);

figure;
opt.Title.String = 'Closed-loop transfer functions from disturbances to outputs';
bode(CL_syn, opt);

figure;
opt.Title.String = 'Weighted closed-loop transfer function';
bode(CL_wFilt, opt);

figure;
opt.Title.String = 'Sensitivity function versus inverse of weighting filter';
bode(CL_syn(1,1), 1/w_d, opt);
legend('Sensitivity function', 'Inverse of w_d');

figure;
opt.Title.String = 'Control sensitivity function versus inverse of weighting filter';
bode(CL_syn(2,2), 1/w_n, opt);
legend('Control sensitivity function', 'Inverse of w_n');

%% Create disturbance spectra

Fs = 1e4;

x_unitNoise = randn(Fs*1000,1);

nAvg = 10;
nfft = length(x_unitNoise)/nAvg;
nOverlap = 0;
WindowType = hann(nfft);

[X_unitNoise, freqVec] = pwelch(x_unitNoise, WindowType, nOverlap, nfft, Fs);

Eta = X_unitNoise.*(1e-5^2);

H_seis_susp_fr = squeeze(freqresp(H_seis_susp, freqVec, 'Hz'));
D_seis = (abs(H_seis_susp_fr).^2).*X_unitNoise;

%% Compute closed-loop system without scaling transfer functions

S_unsc = feedback(1, L_syn);

CL_syn_unsc(1,1) = S_unsc; CL_syn_unsc(1,2) = G*S_unsc;
CL_syn_unsc(2,1) = S_unsc*K_syn; CL_syn_unsc(2,2) = G*K_syn*S_unsc;

%% Compute closed-loop spectra

CL_syn_unsc_fr = freqresp(CL_syn_unsc, freqVec, 'Hz');

E_eta = sqrt((abs(squeeze(CL_syn_unsc_fr(1,1,:))).^2).*Eta);
E_d_seis = sqrt((abs(squeeze(CL_syn_unsc_fr(1,2,:))).^2).*D_seis);
U_eta = sqrt((abs(squeeze(CL_syn_unsc_fr(2,1,:))).^2).*Eta);
U_d_seis = sqrt((abs(squeeze(CL_syn_unsc_fr(2,2,:))).^2).*D_seis);

E = sqrt(E_eta.^2+E_d_seis.^2);
U = sqrt(U_eta.^2+U_d_seis.^2);

binWidth = Fs/nfft;
E_cRMS = sqrt(cumsum((E.^2).*binWidth, 'reverse'));

%% Plot closed-loop spectra

figure;
plot(freqVec, Eta, 'k', 'LineWidth', 2, 'Displayname', '$\eta$')
hold on;
plot(freqVec, D_seis, 'Color', [0.7 0 0], 'LineWidth', 2, 'Displayname', '$D_{\mathrm{seis}}$')
xlabel('Frequency [Hz]')
ylabel('Input disturbances [$\frac{\mathrm{Unit}}{\sqrt{\mathrm{Hz}}}$]', 'Interpreter', 'Latex')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
set(gca, 'FontSize', 16)
xlim([1e-2 1e3]);
grid on;
title('Disturbance spectra')
legend('FontSize', 18, 'Interpreter', 'Latex');

figure;
plot(freqVec, E_eta, 'k', 'LineWidth', 2, 'Displayname', '$E_{\eta}$')
hold on;
plot(freqVec, E_d_seis, 'Color', [0.7 0 0], 'LineWidth', 2, 'Displayname', '$E_{d_{\mathrm{seis}}}$')
xlabel('Frequency [Hz]')
ylabel('Error $e$ [$\frac{\mathrm{W}}{\sqrt{\mathrm{Hz}}}$]', 'Interpreter', 'Latex')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
set(gca, 'FontSize', 16)
xlim([1e-2 1e3]);
grid on;
title(['Error signal, RMS Error = ' num2str(E_cRMS(1)) 'W'])
legend('FontSize', 18, 'Interpreter', 'Latex');

figure;
plot(freqVec, U_eta, 'k', 'LineWidth', 2, 'Displayname', '$U_{\eta}$')
hold on;
plot(freqVec, U_d_seis, 'Color', [0.7 0 0], 'LineWidth', 2, 'Displayname', '$U_{d_{\mathrm{seis}}}$')
xlabel('Frequency [Hz]')
ylabel('Control effort $u$ [$\frac{\mathrm{V}}{\sqrt{\mathrm{Hz}}}$]', 'Interpreter', 'Latex')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
set(gca, 'FontSize', 16)
xlim([1e-2 1e3]);
grid on;
title('Control effort')
legend('FontSize', 20, 'Interpreter', 'Latex');








